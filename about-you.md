## 1. About you

Tell us about one of your commercial projects with Node.js and/or AngularJS.

**Answer**

One of the projects that I am most proud of is called [SessionStack](https://www.sessionstack.com/). 
  
Currently, it is used for enhancing customer experience throughout the support journey.
  
Back in the days, when I was working on the project it was focused on recreating front-end application errors easily. Maybe every front-end developer experience this problem - a customer reports a/an bug/issue but unfortunately, the team can't reproduce it. Then comes the usual questions - which browser are you using, what is the version of the browser, what operating system are you using, can you tell us the exact steps that lead you to the problem, if you open the browser console by clicking F12 do you see any error messages, etc..

The tech stack was AngularJS, pure JavaScript, Node.js, MongoDB, Python (using Flask), MySQL. The project is made from 4 parts:
- UI where the client users can see a video of what exactly happened  to users
- a library which was written in pure JavaScript for capturing the user actions (mouse moves, clicks, etc.)
- Node.js + MongoDB to process those data, find user location, operating system, etc.,
- Python (Flask) REST API + MySQL used for managing accounts, websites, installations of the library, etc..

My job was related to working through the whole stack. Add support for capturing new type of user events, visualize them in the video player, detection of user's location, implementing new REST API endpoints, expiration of user sessions, bug fixing, integration tests setup, and others.
