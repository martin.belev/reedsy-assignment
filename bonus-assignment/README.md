##### Run tests

`npm i && npm test`

Istanbul command line interface `nyc` is used for code coverage. To see the coverage report - run the tests and open `coverage/index.html`.
