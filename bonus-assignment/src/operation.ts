import {
  DeleteOperationType,
  OperationType,
  InsertOperationType,
  MoveCaretOperationType,
  ActionOperationType
} from './operation-type';

export default class Operation {
  private operationTypes: OperationType[];
  private caretPosition: number;

  constructor(operationTypes: OperationType[]) {
    this.operationTypes = [...operationTypes];
    this.caretPosition = 0;
  }

  apply(text: string): string {
    let result: string = text;

    this.caretPosition = 0;
    this.operationTypes.forEach(operationType => {
      result = this.applyEditOperation(result, operationType)
    });

    return result;
  }

  static combine(operation1: Operation, operation2: Operation): Operation {
    const combinedOperationTypes: ActionOperationType[] = this.getCombinedOperationTypes(operation1.operationTypes)
      .concat(this.getCombinedOperationTypes(operation2.operationTypes));

    const groupedActions = this.groupActionsByCaretPosition(combinedOperationTypes);
    const edits = this.mapGroupedActionsToEdits(groupedActions);

    return new Operation(edits);
  }

  combine(operation: Operation): Operation {
    const combinedOperation: Operation = Operation.combine(this, operation);

    this.operationTypes = [...combinedOperation.operationTypes];
    return this;
  }

  private applyEditOperation(text: string, operationType: OperationType): string {
    if (operationType.hasOwnProperty('move')) {
      let moveCaretWith = (operationType as MoveCaretOperationType).move || 0;

      this.caretPosition = Math.min(this.caretPosition + moveCaretWith, text.length);
      if (this.caretPosition < 0) {
        this.caretPosition = 0;
      }

      return text;
    }

    if (operationType.hasOwnProperty('insert')) {
      const insertText = (operationType as InsertOperationType).insert || '';
      const result = text.slice(0, this.caretPosition) + insertText + text.slice(this.caretPosition);

      this.caretPosition += insertText.length;

      return result;
    }

    if (operationType.hasOwnProperty('delete')) {
      let deleteCharactersLength = Math.max((operationType as DeleteOperationType).delete || 0, 0);

      return text.slice(0, this.caretPosition) + text.slice(this.caretPosition + deleteCharactersLength);
    }

    throw new Error('Not supported OperationType');
  }

  private static getCombinedOperationTypes(operationTypes: OperationType[]): ActionOperationType[] {
    const result: ActionOperationType[] = [];

    let caretPosition = 0;

    operationTypes.forEach(operationType => {
      if (operationType.hasOwnProperty('move')) {
        caretPosition += (operationType as MoveCaretOperationType).move || 0;
        return;
      }

      if (operationType.hasOwnProperty('insert')) {
        result.push({
          caretPosition: caretPosition,
          insert: (operationType as InsertOperationType).insert || ''
        });
        return;
      }

      if (operationType.hasOwnProperty('delete')) {
        let deleteCharactersLength = Math.max((operationType as DeleteOperationType).delete || 0, 0);

        result.push({
          caretPosition: caretPosition,
          delete: deleteCharactersLength
        });

        // Needed to support multiple delete operations as the chars are not removed but replaced with *
        caretPosition += deleteCharactersLength;
        return;
      }

      throw new Error('Not supported OperationType');
    });

    return result;
  }

  private static groupActionsByCaretPosition(actions: ActionOperationType[]): {[key: number]: ActionOperationType[]} {
    return actions.reduce((result: {[key: number]: ActionOperationType[]}, item: ActionOperationType) => {
      const value = item.caretPosition;
      result[value] = (result[value] || []).concat(item);
      return result;
    }, {});
  }

  private static mapGroupedActionsToEdits(groupedActions: { [key: string]: ActionOperationType[] }): OperationType[] {
    const result: OperationType[] = [];

    let lastCaretPosition = 0;
    let lastInsertAction: ActionOperationType = null;
    let charsDeletedToPosition = 0;

    Reflect.ownKeys(groupedActions).forEach((caretPositionKey: string) => {
      const caretPosition = +(caretPositionKey);

      groupedActions[caretPosition].forEach((action: ActionOperationType) => {
        if (caretPosition > lastCaretPosition) {
          result.push({ move: caretPosition - lastCaretPosition });
          lastCaretPosition = caretPosition;
        }

        if (action.insert && action.insert.length > 0) {
          const insertText = this.getNextActionInsertText(lastInsertAction, action);
          result.push({ insert: insertText });
          lastInsertAction = action;
        }

        if (action.delete > 0) {
          const charsToDeleteCount = this.getNextActionDeleteCount(charsDeletedToPosition, action);
          result.push({ delete: charsToDeleteCount });
          charsDeletedToPosition = Math.max(charsDeletedToPosition, caretPosition + charsToDeleteCount);
          lastCaretPosition += charsToDeleteCount;
        }
      });
    });

    return result;
  }

  private static getNextActionInsertText(lastAction: ActionOperationType, nextAction: ActionOperationType): string {
    if (!lastAction || lastAction.caretPosition !== nextAction.caretPosition) {
      return nextAction.insert;
    }

    const nextInsertText = nextAction.insert;
    // Do not insert the same text on the same position
    if (lastAction.insert === nextInsertText) {
      return '';
    }

    let result = nextAction.insert;

    // Do not add repetitive text
    // if next insert operation text was in the previous insert operation
    // take only the part of the next insert operation text that wasn't added previously
    if (lastAction.insert.startsWith(nextInsertText) || nextInsertText.startsWith((lastAction.insert))) {
      if (lastAction.insert.length >= nextInsertText.length) {
        result = '';
      } else {
        result = nextInsertText.substring(lastAction.insert.length);
      }
    }

    return result;
  }

  private static getNextActionDeleteCount(charsDeletedToPosition: number, nextAction: ActionOperationType): number {
    if (charsDeletedToPosition === 0) {
      return nextAction.delete;
    }

    const nextDeletedCharsTo = nextAction.delete + nextAction.caretPosition;
    return Math.max(0, nextDeletedCharsTo - charsDeletedToPosition);
  }
}
