export type MoveCaretOperationType = {
  move: number
}

export type InsertOperationType = {
  insert: string
}

export type DeleteOperationType = {
  delete: number;
}

export type OperationType = MoveCaretOperationType | InsertOperationType | DeleteOperationType;

export type ActionOperationType = {
  caretPosition: number,
  insert?: string,
  delete?: number
}
