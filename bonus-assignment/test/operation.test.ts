import { assert } from 'chai';
import Operation from '../src/operation';

describe('Not combined operation apply', () => {
  describe('insert', () => {
    it('with text', () => {
      const operation = new Operation([{ insert: 'some text' }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'some textabc');
    });

    it('change caret position and add next text', () => {
      const operation = new Operation([{ insert: 'some text' }, { insert: ' other ' }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'some text other abc');
    });

    it('with empty text', () => {
      const operation = new Operation([{ insert: '' }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'abc');
    });

    it('with null value', () => {
      const operation = new Operation([{ insert: null }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'abc');
    });
  });

  describe('delete', () => {
    it('with positive number', () => {
      const operation = new Operation([{ delete: 2 }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'c');
    });

    it('with multiple positive numbers', () => {
      const operation = new Operation([{ delete: 1 }, { delete: 1 }, { delete: 1 }, { delete: 1 }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, '');
    });

    it('with positive number greater than text length', () => {
      const operation = new Operation([{ delete: 5 }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, '');
    });

    it('with negative number', () => {
      const operation = new Operation([{ delete: -5 }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'abc');
    });

    it('with null value', () => {
      const operation = new Operation([{ delete: null }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'abc');
    });
  });

  describe('move', () => {
    it('with positive number', () => {
      const operation = new Operation([{ move: 2 }, { insert: 'z' }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'abzc');
    });

    it('with positive number greater than text length', () => {
      const operation = new Operation([{ move: 5 }, { insert: 'z' }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'abcz');
    });

    it('with negative number', () => {
      const operation = new Operation([{ move: -1 }, { insert: 'z' }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'zabc');
    });

    it('with positive number and with negative number', () => {
      const operation = new Operation([{ move: 3 }, { insert: 'z' }, { move: -2}, { insert: 'y' }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'abycz');
    });

    it('with null value', () => {
      const operation = new Operation([{ move: null }, { insert: 'z' }]);
      const text = 'abc';

      const result = operation.apply(text);

      assert.equal(result, 'zabc');
    });
  });

  it('with combination of edits', () => {
    const operation = new Operation([
      { move: 3 }, { insert: 'z' }, { move: -2}, { insert: 'y' },
      { insert: 'text' }, { delete: 2 }, { insert: ' another text' },
      { move: -5 }, { delete: 1 }, { insert: 'glued text' }
    ]);
    const text = 'abc';

    const result = operation.apply(text);

    assert.equal(result, 'abytext anotherglued texttext');
  });
});

describe('Combine two operations and apply', () => {
  it('with insert', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
    const operation2 = new Operation([{ move: 3 }, { insert: 'BAR' }]);
    operation1.combine(operation2);

    const result = operation1.apply(text);

    assert.equal(result, 'aFOObcBARdefg');
  });

  it('with same insert', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
    const operation2 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
    operation1.combine(operation2);

    const result = operation1.apply(text);

    assert.equal(result, 'aFOObcdefg');
  });

  it('with multiple inserts', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { insert: 'FOO' }, { move: 1 }, { insert: 'Y' }, { move: 1 }, { insert: 'Z' }]);
    const operation2 = new Operation([{ move: 3 }, { insert: 'BAR' }, { move: 1 }, { insert: 'M' }, { move: 1 }, { insert: 'N' }]);
    operation1.combine(operation2);

    const result = operation1.apply(text);

    assert.equal(result, 'aFOObYcZBARdMeNfg');
  });

  it('with delete', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { delete: 4 }]);
    const operation2 = new Operation([{ move: 2 }, { delete: 4 }]);
    operation1.combine(operation2);

    const result = operation1.apply(text);

    assert.equal(result, 'ag');
  });

  it('with same delete', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { delete: 2 }]);
    const operation2 = new Operation([{ move: 1 }, { delete: 2 }]);
    operation1.combine(operation2);

    const result = operation1.apply(text);

    assert.equal(result, 'adefg');
  });

  it('with multiple deletes', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { delete: 2 }, { move: 1 }, { delete: 1 }, { move: 1 }, { delete: 1 }]);
    const operation2 = new Operation([{ delete: 1 }, { move: 4 }, { delete: 2 }]);
    operation1.combine(operation2);

    const result = operation1.apply(text);

    assert.equal(result, 'd');
  });

  it('with deletes to end of file and insert', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { delete: 2 }, { move: 1 }, { delete: 1 }, { move: 1 }, { delete: 1 }, { insert: 'operation1' }]);
    const operation2 = new Operation([{ delete: 1 }, { move: 4 }, { delete: 2 }, { insert: 'operation2' }]);
    operation1.combine(operation2);

    const result = operation1.apply(text);

    assert.equal(result, 'doperation1operation2');
  });
});

describe('Static combine two operations and apply', () => {
  it('with insert', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
    const operation2 = new Operation([{ move: 3 }, { insert: 'BAR' }]);
    const operation = Operation.combine(operation1, operation2);

    const result = operation.apply(text);

    assert.equal(result, 'aFOObcBARdefg');
  });

  it('with same insert', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
    const operation2 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
    const operation = Operation.combine(operation1, operation2);

    const result = operation.apply(text);

    assert.equal(result, 'aFOObcdefg');
  });

  it('with insert starts with longer collided insert', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
    const operation2 = new Operation([{ move: 1 }, { insert: 'FOOFOO' }]);
    const operation = Operation.combine(operation1, operation2);

    const result = operation.apply(text);

    assert.equal(result, 'aFOOFOObcdefg');
  });

  it('with insert starts with shorter collided insert', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { insert: 'FOOFOO' }]);
    const operation2 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
    const operation = Operation.combine(operation1, operation2);

    const result = operation.apply(text);

    assert.equal(result, 'aFOOFOObcdefg');
  });

  it('with multiple inserts', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { insert: 'FOO' }, { move: 1 }, { insert: 'Y' }, { move: 1 }, { insert: 'Z' }]);
    const operation2 = new Operation([{ move: 3 }, { insert: 'BAR' }, { move: 1 }, { insert: 'M' }, { move: 1 }, { insert: 'N' }]);
    const operation = Operation.combine(operation1, operation2);

    const result = operation.apply(text);

    assert.equal(result, 'aFOObYcZBARdMeNfg');
  });

  it('with delete', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { delete: 4 }]);
    const operation2 = new Operation([{ move: 2 }, { delete: 4 }]);
    const operation = Operation.combine(operation1, operation2);

    const result = operation.apply(text);

    assert.equal(result, 'ag');
  });

  it('with same delete', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { delete: 2 }]);
    const operation2 = new Operation([{ move: 1 }, { delete: 2 }]);
    const operation = Operation.combine(operation1, operation2);

    const result = operation.apply(text);

    assert.equal(result, 'adefg');
  });

  it('with multiple deletes', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { delete: 2 }, { move: 1 }, { delete: 1 }, { move: 1 }, { delete: 1 }]);
    const operation2 = new Operation([{ delete: 1 }, { move: 4 }, { delete: 2 }]);
    const operation = Operation.combine(operation1, operation2);

    const result = operation.apply(text);

    assert.equal(result, 'd');
  });

  it('with deletes to end of file and insert', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ move: 1 }, { delete: 2 }, { move: 1 }, { delete: 1 }, { move: 1 }, { delete: 1 }, { insert: 'operation1' }]);
    const operation2 = new Operation([{ delete: 1 }, { move: 4 }, { delete: 2 }, { insert: 'operation2' }]);
    const operation = Operation.combine(operation1, operation2);

    const result = operation.apply(text);

    assert.equal(result, 'doperation1operation2');
  });

  it('with deletes and inserts', () => {
    let text = 'abcdefg';

    const operation1 = new Operation([{ insert: 'A' }, { move: 1 }, { delete: 2 }, { move: 1 }, { delete: 1 }, { insert: 'B' }, { move: 1 }, { delete: 1 }, { insert: 'C' }]);
    const operation2 = new Operation([{ delete: 1 }, { insert: 'X' }, { move: 4 }, { insert: 'Y' }, { delete: 2 }, { insert: 'Z' }]);
    const operation = Operation.combine(operation1, operation2);

    const result = operation.apply(text);

    assert.equal(result, 'AXdBYCZ');
  });
});

it('combine multiple operations and apply', () => {
  let text = 'abcdefg';

  const operation1 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
  const operation2 = new Operation([{ move: 3 }, { insert: 'BAR' }]);

  const operation3 = new Operation([{ delete: 1 }, { insert: 'Z' }, { move: 5 }, { insert: 'Y' }, { delete: 1 }]);
  const operation = Operation.combine(operation1, operation2).combine(operation3);

  const result = operation.apply(text);

  assert.equal(result, 'FOOZbcBARdefY');
});

it('combine same multiple operations and apply', () => {
  let text = 'abcdefg';

  const operation1 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
  const operation2 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
  const operation3 = new Operation([{ move: 1 }, { insert: 'FOO' }]);
  const operation4 = new Operation([{ move: 1 }, { insert: 'FOO' }]);

  const operation12 = Operation.combine(operation1, operation2);
  const operation34 = Operation.combine(operation3, operation4);
  operation12.combine(operation34);

  const result = operation12.apply(text);

  assert.equal(result, 'aFOObcdefg');
});
