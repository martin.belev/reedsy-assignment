import chai, { assert } from 'chai';
import chaiHttp from 'chai-http';

import { app } from '../src/app';
import ExportJob, { ExportJobType } from '../src/models/export-job.model';
import { JobState } from '../src/models/job-state';

chai.use(chaiHttp);

describe('Exports REST API', () => {
  before(async () => {
    await app.start();
  });

  after(async () => {
    await app.stop();
  });

  afterEach(() => {
    app.exportJobService.clear();
  });

  it('/GET exports', testDone => {
    app.exportJobService.createExportJob('book id 1', ExportJobType.Pdf);

    chai.request(app.webApp)
      .get('/api/exports')
      .then(res => {
        const pendingExports = res.body[JobState.Pending];
        const finishedExports = res.body[JobState.Finished];
        assert.isArray(pendingExports);
        assert.isArray(finishedExports);
        assert.equal(pendingExports.length, 1);
        assert.equal(finishedExports.length, 0);

        const result = pendingExports[0];
        assert.equal(result.bookId, 'book id 1');
        assert.equal(result.type, ExportJobType.Pdf);
        assert.equal(result.state, JobState.Pending);
        assert.isDefined(result.created_at);

        testDone();
      });
  });

  it('/POST exports - valid body', testDone => {
    chai.request(app.webApp)
      .post('/api/exports')
      .send({ bookId: 'book id 1', type: ExportJobType.Pdf })
      .then(res => {
        assert.isObject(res.body);

        const result = res.body;
        assert.equal(result.bookId, 'book id 1');
        assert.equal(result.type, ExportJobType.Pdf);
        assert.equal(result.state, JobState.Pending);
        assert.isDefined(result.created_at);

        testDone();
      });
  });

  it('/POST exports - missing bookId', testDone => {
    chai.request(app.webApp)
      .post('/api/exports')
      .send({ type: ExportJobType.Pdf })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST exports - empty bookId', testDone => {
    chai.request(app.webApp)
      .post('/api/exports')
      .send({ bookId: '  ', type: ExportJobType.Pdf })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST exports - missing type', testDone => {
    chai.request(app.webApp)
      .post('/api/exports')
      .send({ bookId: 'book id 1' })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST exports - empty type', testDone => {
    chai.request(app.webApp)
      .post('/api/exports')
      .send({ bookId: 'book id 1', type: '     ' })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST exports - invalid type value', testDone => {
    chai.request(app.webApp)
      .post('/api/exports')
      .send({ bookId: 'book id 1', type: 'invalid type' })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST exports - added extra field', testDone => {
    chai.request(app.webApp)
      .post('/api/exports')
      .send({ bookId: 'book id 1', type: ExportJobType.Pdf, someField: 5 })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });
});
