import chai, { assert } from 'chai';
import chaiHttp from 'chai-http';

import { app } from '../src/app';
import ImportJob, { ImportJobType } from '../src/models/import-job.model';
import { JobState } from '../src/models/job-state';

chai.use(chaiHttp);

describe('Imports REST API', () => {
  before(async () => {
    await app.start();
  });

  after(async () => {
    await app.stop();
  });

  afterEach(() => {
    app.importJobService.clear();
  });

  it('/GET imports', testDone => {
    app.importJobService.createImportJob('book id 1', ImportJobType.Pdf, 'https://google.com');

    chai.request(app.webApp)
      .get('/api/imports')
      .then(res => {
        const pendingImports = res.body[JobState.Pending];
        const finishedImports = res.body[JobState.Finished];
        assert.isArray(pendingImports);
        assert.isArray(finishedImports);
        assert.equal(pendingImports.length, 1);
        assert.equal(finishedImports.length, 0);

        const result = pendingImports[0];
        assert.equal(result.bookId, 'book id 1');
        assert.equal(result.type, ImportJobType.Pdf);
        assert.equal(result.url, 'https://google.com');
        assert.equal(result.state, JobState.Pending);
        assert.isDefined(result.created_at);

        testDone();
      });
  });

  it('/POST imports - valid body', testDone => {
    chai.request(app.webApp)
      .post('/api/imports')
      .send({ bookId: 'book id 1', type: ImportJobType.Pdf, url: 'https://google.com' })
      .then(res => {
        assert.isObject(res.body);

        const result = res.body;
        assert.equal(result.bookId, 'book id 1');
        assert.equal(result.type, ImportJobType.Pdf);
        assert.equal(result.url, 'https://google.com');
        assert.equal(result.state, JobState.Pending);
        assert.isDefined(result.created_at);

        testDone();
      });
  });

  it('/POST imports - missing bookId', testDone => {
    chai.request(app.webApp)
      .post('/api/imports')
      .send({ type: ImportJobType.Pdf })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST imports - empty bookId', testDone => {
    chai.request(app.webApp)
      .post('/api/imports')
      .send({ bookId: '  ', type: ImportJobType.Pdf })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST imports - missing type', testDone => {
    chai.request(app.webApp)
      .post('/api/imports')
      .send({ bookId: 'book id 1' })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST imports - empty type', testDone => {
    chai.request(app.webApp)
      .post('/api/imports')
      .send({ bookId: 'book id 1', type: '     ' })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST imports - invalid type value', testDone => {
    chai.request(app.webApp)
      .post('/api/imports')
      .send({ bookId: 'book id 1', type: 'invalid type' })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST imports - missing url', testDone => {
    chai.request(app.webApp)
      .post('/api/imports')
      .send({ bookId: 'book id 1', type: ImportJobType.Pdf })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST imports - invalid url', testDone => {
    chai.request(app.webApp)
      .post('/api/imports')
      .send({ bookId: 'book id 1', type: ImportJobType.Pdf, url: 'invalid url' })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });

  it('/POST imports - added extra field', testDone => {
    chai.request(app.webApp)
      .post('/api/imports')
      .send({ bookId: 'book id 1', type: ImportJobType.Pdf, someField: 5 })
      .then(res => {
        assert.equal(res.status, 400);
        assert.isDefined(res.body.error);

        testDone();
      });
  });
});
