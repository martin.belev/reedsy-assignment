import { assert } from 'chai';
import * as sinon from 'sinon';

import ImportJobService from '../../src/services/import-job.service';
import ImportJob, { ImportJobType } from '../../src/models/import-job.model';
import { JobState } from '../../src/models/job-state';

describe('ImportJobService', () => {
  let importJobService: ImportJobService;

  before(() => {
    importJobService = new ImportJobService();
  });

  afterEach(() => {
    importJobService.clear();
  });

  it('createImportJob - return created job', () => {
    const result: ImportJob = importJobService.createImportJob('book id 1', ImportJobType.Pdf, 'https://google.com');

    assert.equal(result.getBookId(), 'book id 1');
    assert.equal(result.getType(), ImportJobType.Pdf);
    assert.equal(result.getUrl(), 'https://google.com');
    assert.equal(result.getState(), JobState.Pending);
    assert.isAtMost(result.getCreatedAt(), new Date().getTime());
    assert.isUndefined(result.getUpdatedAt());
  });

  it('createImportJob - process created job', () => {
    const processJobSpy = sinon.spy(importJobService, <any>'processJob');
    const result: ImportJob = importJobService.createImportJob('book id 1', ImportJobType.Pdf, 'https://google.com');

    assert.isTrue(processJobSpy.calledOnce);
    assert.isTrue(processJobSpy.calledWithExactly(result));
  });

  it('getImportJobs - group by state', () => {
    const import1: ImportJob = importJobService.createImportJob('book id 1', ImportJobType.Pdf, 'https://google.com');
    const import2: ImportJob = importJobService.createImportJob('book id 2', ImportJobType.Word, 'https://gmail.com');
    const import3: ImportJob = importJobService.createImportJob('book id 2', ImportJobType.Wattpad, 'https://abv.bg');
    import3.setState(JobState.Finished);

    const result = importJobService.getImportJobs();

    assert.deepEqual(result[JobState.Pending], [import1, import2]);
    assert.deepEqual(result[JobState.Finished], [import3]);
  });

  it('clear', () => {
    importJobService.createImportJob('book id 1', ImportJobType.Pdf, 'https://google.com');
    importJobService.createImportJob('book id 2', ImportJobType.Word, 'https://gmail.com');
    importJobService.createImportJob('book id 2', ImportJobType.Wattpad, 'https://abv.bg');

    importJobService.clear();

    const result = importJobService.getImportJobs();

    assert.isEmpty(result[JobState.Pending]);
    assert.isEmpty(result[JobState.Finished]);
  })
});
