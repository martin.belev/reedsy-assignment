import { assert } from 'chai';
import * as sinon from 'sinon';

import ExportJobService from '../../src/services/export-job.service';
import ExportJob, { ExportJobType } from '../../src/models/export-job.model';
import { JobState } from '../../src/models/job-state';

describe('ExportJobService', () => {
  let exportJobService: ExportJobService;

  before(() => {
    exportJobService = new ExportJobService();
  });

  afterEach(() => {
    exportJobService.clear();
  });

  it('createExportJob - return created job', () => {
    const result: ExportJob = exportJobService.createExportJob('book id 1', ExportJobType.Pdf);

    assert.equal(result.getBookId(), 'book id 1');
    assert.equal(result.getType(), ExportJobType.Pdf);
    assert.equal(result.getState(), JobState.Pending);
    assert.isAtMost(result.getCreatedAt(), new Date().getTime());
    assert.isUndefined(result.getUpdatedAt());
  });

  it('createExportJob - process created job', () => {
    const processJobSpy = sinon.spy(exportJobService, <any>'processJob');
    const result: ExportJob = exportJobService.createExportJob('book id 1', ExportJobType.Pdf);

    assert.isTrue(processJobSpy.calledOnce);
    assert.isTrue(processJobSpy.calledWithExactly(result));
  });

  it('getExportJobs - group by state', () => {
    const export1: ExportJob = exportJobService.createExportJob('book id 1', ExportJobType.Pdf);
    const export2: ExportJob = exportJobService.createExportJob('book id 2', ExportJobType.Epub);
    const export3: ExportJob = exportJobService.createExportJob('book id 2', ExportJobType.Epub);
    export3.setState(JobState.Finished);

    const result = exportJobService.getExportJobs();

    assert.deepEqual(result[JobState.Pending], [export1, export2]);
    assert.deepEqual(result[JobState.Finished], [export3]);
  });

  it('clear', () => {
    exportJobService.createExportJob('book id 1', ExportJobType.Pdf);
    exportJobService.createExportJob('book id 2', ExportJobType.Epub);
    exportJobService.createExportJob('book id 2', ExportJobType.Epub);

    exportJobService.clear();

    const result = exportJobService.getExportJobs();

    assert.isEmpty(result[JobState.Pending]);
    assert.isEmpty(result[JobState.Finished]);
  })
});
