import { ExportJobType } from '../models/export-job.model';

export const exportJobProcessingTime = {
  [ExportJobType.Epub]: 10 * 1000,
  [ExportJobType.Pdf]: 25 * 1000
};
