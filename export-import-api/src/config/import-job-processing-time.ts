import { ImportJobType } from '../models/import-job.model';

export const importJobProcessingTime = {
  [ImportJobType.Word]: 60 * 1000,
  [ImportJobType.Pdf]: 60 * 1000,
  [ImportJobType.Wattpad]: 60 * 1000,
  [ImportJobType.Evernote]: 60 * 1000,
};
