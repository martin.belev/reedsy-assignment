import express from 'express';
import ExportJobController from './controllers/export-job.controller';
import ImportJobController from './controllers/import-job.controller';

export default class Router {
  readonly routes: express.Router;

  constructor(
    exportJobController: ExportJobController,
    importJobController: ImportJobController
  ) {
    this.routes = express.Router();

    this.routes.route('/exports')
      .post((req: express.Request, res: express.Response) => exportJobController.createExportJob(req, res))
      .get((req: express.Request, res: express.Response) => exportJobController.getExportJobs(req, res));

    this.routes.route('/imports')
      .post((req: express.Request, res: express.Response) => importJobController.createImportJob(req, res))
      .get((req: express.Request, res: express.Response) => importJobController.getImportJobs(req, res));
  }
}
