import ImportJob, { ImportJobType } from '../models/import-job.model';
import { JobState } from '../models/job-state';
import { importJobProcessingTime } from '../config/import-job-processing-time';
import { clearTimeout } from 'timers';
import { groupBy } from '../utils/utils';
import Timeout = NodeJS.Timeout;

export default class ImportJobService {
  private jobs: ImportJob[];
  private jobTimeouts: Timeout[];

  constructor() {
    this.jobs = [];
    this.jobTimeouts = [];
  }

  getImportJobs(): {[key: string]: ImportJob[]} {
    return groupBy(this.jobs, 'state', [JobState.Pending, JobState.Finished]);
  }

  createImportJob(bookId: string, type: ImportJobType, url: string): ImportJob {
    const job = new ImportJob(bookId, type, url);
    this.jobs.push(job);

    this.processJob(job);

    return job;
  }

  clear(): void {
    this.jobTimeouts.forEach(timeout => clearTimeout(timeout));
    this.jobs = [];
  }

  private processJob(job: ImportJob): void {
    const timeout = setTimeout(() => {
      job.setState(JobState.Finished);
      job.setUpdatedAt(new Date().getTime());

      this.jobTimeouts = this.jobTimeouts.filter(t => t !== timeout);
    }, importJobProcessingTime[job.getType()]);

    this.jobTimeouts.push(timeout);
  }
}
