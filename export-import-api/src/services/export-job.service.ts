import ExportJob, { ExportJobType } from '../models/export-job.model';
import { JobState } from '../models/job-state';
import { exportJobProcessingTime } from '../config/export-job-processing-time';
import Timeout = NodeJS.Timeout;
import { clearTimeout } from 'timers';
import { groupBy } from '../utils/utils';

export default class ExportJobService {
  private jobs: ExportJob[];
  private jobTimeouts: Timeout[];

  constructor() {
    this.jobs = [];
    this.jobTimeouts = [];
  }

  getExportJobs(): {[key: string]: ExportJob[]} {
    return groupBy(this.jobs, 'state', [JobState.Pending, JobState.Finished]);
  }

  createExportJob(bookId: string, type: ExportJobType): ExportJob {
    const job = new ExportJob(bookId, type);
    this.jobs.push(job);

    this.processJob(job);

    return job;
  }

  clear(): void {
    this.jobTimeouts.forEach(timeout => clearTimeout(timeout));
    this.jobs = [];
  }

  private processJob(job: ExportJob): void {
    const timeout = setTimeout(() => {
      job.setState(JobState.Finished);
      job.setUpdatedAt(new Date().getTime());

      this.jobTimeouts = this.jobTimeouts.filter(t => t !== timeout);
    }, exportJobProcessingTime[job.getType()]);

    this.jobTimeouts.push(timeout);
  }
}
