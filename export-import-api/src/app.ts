import express from 'express';
import * as bodyParser from 'body-parser';
import * as http from "http";

import ExportJobService from './services/export-job.service';
import ExportJobController from './controllers/export-job.controller';
import Router from './router';
import ImportJobController from './controllers/import-job.controller';
import ImportJobService from './services/import-job.service';
import RequestError from './models/request-error.model';

class App {
  public readonly  exportJobService: ExportJobService;
  public readonly  importJobService: ImportJobService;

  public readonly  exportJobController: ExportJobController;
  public readonly  importJobController: ImportJobController;

  public readonly webApp: express.Express;
  private server: http.Server;

  private readonly port: number;
  private router: Router;

  constructor(port?: number) {
    this.exportJobService = new ExportJobService();
    this.importJobService = new ImportJobService();

    this.exportJobController = new ExportJobController(this.exportJobService);
    this.importJobController = new ImportJobController(this.importJobService);

    this.port =  parseInt(<string>process.env.PORT, 10) || port || 4000;
    this.router = new Router(this.exportJobController, this.importJobController);

    this.webApp = express();
    this.webApp.use(bodyParser.json());
    this.webApp.use('/api', this.router.routes);

    this.webApp.use((req: express.Request, res: express.Response) => {
      res.status(404).json({ message: 'Not found' });
    });

    this.webApp.use((err: RequestError, req: express.Request, res: express.Response, next: express.NextFunction) => {
      if (res.headersSent) {
        return next(err);
      }

      res.status(err.statusCode || 500);
      res.json({ message: err.message || 'Oops! Something went wrong!' });
    });
  }

  start(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.server = this.webApp.listen(this.port, (err: Error, result: any) => {
        if (err) {
          return reject(err);
        }
        console.log("Running HTTP on port: " + this.port);
        resolve(result);
      });
    });
  }

  stop(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.server.close(err => {
        if (err) {
          return reject(err);
        }
        resolve();
      });
    });
  }
}

export const app = new App();
