export enum JobState {
  Pending = 'pending',
  Finished = 'finished'
}
