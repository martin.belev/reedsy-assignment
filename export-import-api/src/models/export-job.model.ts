import { JobState } from './job-state';

export enum ExportJobType {
  Epub = 'epub',
  Pdf = 'pdf'
}

export default class ExportJob {
  private bookId: string;
  private type: ExportJobType;
  private state: JobState;
  private created_at: number;
  private updated_at?: number;

  constructor(bookId: string, type: ExportJobType) {
    this.bookId = bookId;
    this.type = type;
    this.state = JobState.Pending;
    this.created_at = new Date().getTime();
  }

  getBookId(): string {
    return this.bookId;
  }

  getType(): ExportJobType {
    return this.type;
  }

  getState(): JobState {
    return this.state;
  }

  setState(state: JobState): void {
    this.state = state;
  }

  getCreatedAt(): number {
    return this.created_at;
  }

  getUpdatedAt(): number {
    return this.updated_at;
  }

  setUpdatedAt(updatedAt: number) {
    this.updated_at = updatedAt;
  }
}
