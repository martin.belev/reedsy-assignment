export default interface RequestError {
  message?: string;
  statusCode?: number;
}
