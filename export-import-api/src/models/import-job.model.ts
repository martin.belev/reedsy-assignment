import { JobState } from './job-state';

export enum ImportJobType {
  Word = 'word',
  Pdf = 'pdf',
  Wattpad = 'wattpad',
  Evernote = 'evernote'
}

export default class ImportJob {
  private bookId: string;
  private type: ImportJobType;
  private url: string;
  private state: JobState;
  private created_at: number;
  private updated_at?: number;

  constructor(bookId: string, type: ImportJobType, url: string) {
    this.bookId = bookId;
    this.type = type;
    this.url = url;
    this.state = JobState.Pending;
    this.created_at = new Date().getTime();
  }

  getBookId(): string {
    return this.bookId;
  }

  getType(): ImportJobType {
    return this.type;
  }

  getUrl(): string {
    return this.url;
  }

  getState(): JobState {
    return this.state;
  }

  setState(state: JobState): void {
    this.state = state;
  }

  getCreatedAt(): number {
    return this.created_at;
  }

  getUpdatedAt(): number {
    return this.updated_at;
  }

  setUpdatedAt(updatedAt: number) {
    this.updated_at = updatedAt;
  }
}
