const Joi = require('@hapi/joi');

import express from 'express';

import ImportJobService from '../services/import-job.service';
import { importJobSchema } from './schemas/import-job.schema';

export default class ImportJobController {
  private readonly importJobService: ImportJobService;

  constructor(ImportJobService: ImportJobService) {
    this.importJobService = ImportJobService;
  }

  getImportJobs(req: express.Request, res: express.Response) {
    const result = this.importJobService.getImportJobs();

    res.json(result);
  }

  createImportJob(req: express.Request, res: express.Response) {
    const validatedBody = Joi.validate(req.body, importJobSchema);
    if (validatedBody.error) {
      return res.status(400).json({ error: validatedBody.error.details[0].message });
    }

    const result = this.importJobService.createImportJob(validatedBody.value.bookId, validatedBody.value.type, validatedBody.value.url);
    res.json(result);
  }
}
