const Joi = require('@hapi/joi');

import express from 'express';

import ExportJobService from '../services/export-job.service';
import { exportJobSchema } from './schemas/export-job.schema';

export default class ExportJobController {
  private readonly exportJobService: ExportJobService;

  constructor(exportJobService: ExportJobService) {
    this.exportJobService = exportJobService;
  }

  getExportJobs(req: express.Request, res: express.Response) {
    const result = this.exportJobService.getExportJobs();

    res.json(result);
  }

  createExportJob(req: express.Request, res: express.Response) {
    const validatedBody = Joi.validate(req.body, exportJobSchema);
    if (validatedBody.error) {
      return res.status(400).json({ error: validatedBody.error.details[0].message });
    }

    const result = this.exportJobService.createExportJob(validatedBody.value.bookId, validatedBody.value.type);
    res.json(result);
  }
}
