const Joi = require('@hapi/joi');

import { ImportJobType } from '../../models/import-job.model';

export const importJobSchema = Joi.object({
  bookId: Joi.string().trim().required(),
  type: Joi.string().valid([ImportJobType.Word, ImportJobType.Pdf, ImportJobType.Wattpad, ImportJobType.Evernote]).required(),
  url: Joi.string().uri().trim().required()
});
