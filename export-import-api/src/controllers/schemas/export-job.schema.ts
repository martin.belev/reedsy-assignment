const Joi = require('@hapi/joi');

import { ExportJobType } from '../../models/export-job.model';

export const exportJobSchema = Joi.object({
  bookId: Joi.string().trim().required(),
  type: Joi.string().valid([ExportJobType.Epub, ExportJobType.Pdf]).required()
});
