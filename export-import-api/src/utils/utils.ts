export function groupBy<T>(array: T[], key: string, possibleKeyValues?: string[]): {[key: string]: T[]} {
  const result = array.reduce((result, item) => {
    const value = item[key];
    result[value] = (result[value] || []).concat(item);
    return result;
  }, {});

  if (possibleKeyValues) {
    possibleKeyValues.forEach(value => result[value] = result[value] || []);
  }

  return result;
}
