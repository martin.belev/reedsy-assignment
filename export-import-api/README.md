### REST API for export/import job requests

Used Node.js LTS version (v10.16.0 at the moment of implementation).

##### Run server

`npm i && npm start`

##### Run tests

`npm i && npm test`

Istanbul command line interface `nyc` is used for code coverage. To see the coverage report - run the tests and open `coverage/index.html`.

##### Notes

The given implementation of handling job requests is the simplest one with the usage of `setTimeout` only.
It can be improved a lot, for sure, but I decided not to complicate things.

- One way would be to have job queue that is processing the requests, for example up to 5-6 requests simultaneously. Another `state` will be introduced then - `processing`.
- If the service should be really scalable, the execution of the job request can be extracted into AWS Lambda or similar provider. It will be called with some unique identifier and redirect URL. The call will terminate the HTTP connection as soon as possible. When the lambda process the specified job request, it will make a call to the redirect URL to notify the server that the specified job request has finished.
