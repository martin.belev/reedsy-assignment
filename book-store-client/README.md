# Book store

The [angularjs-webpack-starter](https://github.com/frederikprijck/angularjs-webpack-starter) was used as starting point.

## Usage
```
 npm install
```

Most useful npm scripts that can be used:

```
npm start
npm run test
npm run test:coverage
```
