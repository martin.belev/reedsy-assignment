// temporary, until https://github.com/Microsoft/TypeScript/issues/10178 is implemented
import * as angular from 'angular';

import { BooksContainer } from './components/books/books.container';
import { configuration } from './app.configuration';
import { routing } from './app.routes';
import { App } from './app.component';
import { BooksService } from './services/books.service';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { BookListComponent } from './components/books/book-list/book-list.component';
import { TrustHtmlFilter } from './filters/trust-html.filter';

export const moduleName = angular.module('application', [
    'ui.router'
  ])

  .component(App.selector, App)
  .component(BooksContainer.selector, BooksContainer)
  .component(PaginatorComponent.selector, PaginatorComponent)
  .component(BookListComponent.selector, BookListComponent)

  .service(BooksService.selector, BooksService)

  .filter(TrustHtmlFilter.selector, TrustHtmlFilter.filter)

  .config(configuration)
  .config(routing)

  .name;
