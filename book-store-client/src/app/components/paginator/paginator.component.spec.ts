import * as angular from 'angular';
import 'angular-mocks';
import { PaginatorComponent } from './paginator.component';

describe('Paginator component', () => {
  beforeEach(() => {
    angular
      .module('app', [])
      .component(PaginatorComponent.selector, PaginatorComponent);
    angular.mock.module('app');
  });

  it('should exist', angular.mock.inject(($componentController: any) => {
    const component = $componentController(PaginatorComponent.selector, {}, {});

    expect(component).toBeDefined();
  }));

  it('should pass `data` to `pageChanged` binding when page is changed to next page', angular.mock.inject(($componentController: any) => {
    const bindings = {
      pageChanged: jasmine.createSpy('pageChanged')
    };
    const component = $componentController(PaginatorComponent.selector, {}, bindings);
    component.nextPage();

    expect(bindings.pageChanged).toHaveBeenCalledWith({ $event: { data: { pageIndex: 1, pageSize: 5 }}});
  }));

  it('should not call `pageChanged` binding when pageIndex is last page and go to next page', angular.mock.inject(($componentController: any) => {
    const bindings = {
      pageChanged: jasmine.createSpy('pageChanged'),
      pageIndex: 2,
      totalElements: 11
    };
    const component = $componentController(PaginatorComponent.selector, {}, bindings);
    component.$onChanges();
    component.nextPage();

    expect(bindings.pageChanged).not.toHaveBeenCalled();
  }));

  it('should pass `data` to `pageChanged` binding when page is changed to previous page', angular.mock.inject(($componentController: any) => {
    const bindings = {
      pageChanged: jasmine.createSpy('pageChanged'),
      pageIndex: 1
    };
    const component = $componentController(PaginatorComponent.selector, {}, bindings);
    component.previousPage();

    expect(bindings.pageChanged).toHaveBeenCalledWith({ $event: { data: { pageIndex: 0, pageSize: 5 }}});
  }));

  it('should not call `pageChanged` binding when pageIndex is 0 and go to previous page', angular.mock.inject(($componentController: any) => {
    const bindings = {
      pageChanged: jasmine.createSpy('pageChanged')
    };
    const component = $componentController(PaginatorComponent.selector, {}, bindings);
    component.previousPage();

    expect(bindings.pageChanged).not.toHaveBeenCalled();
  }));
});
