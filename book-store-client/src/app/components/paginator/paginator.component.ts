import './paginator.component.scss';

class PaginatorController implements angular.IComponentController {
  pageIndex: number;
  pageSize: number;
  totalElements: number;
  pageChanged: ($event: { $event: {data: { pageIndex: number, pageSize: number }} }) => void;

  totalPages: number;

  constructor() {
    this.pageIndex = 0;
    this.pageSize = 5;
    this.totalElements = 5;
  }

  $onChanges(onChangesObj: angular.IOnChangesObject): void {
    this.totalPages = Math.ceil(this.totalElements / this.pageSize);
  }

  nextPage() {
    if (this.pageIndex === this.totalPages - 1) {
      return;
    }
    this.changePage(this.pageIndex + 1);
  }

  previousPage() {
    if (this.pageIndex === 0) {
      return;
    }
    this.changePage(this.pageIndex - 1);
  }

  private changePage(pageIndex: number = this.pageIndex, pageSize: number = this.pageSize): void {
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.pageChanged({
      $event: {
        data: { pageIndex, pageSize }
      }
    });
  }
}


export class PaginatorComponent implements angular.IComponentOptions {
  static selector = 'paginator';
  static bindings = {
    pageIndex: '<',
    pageSize: '<',
    totalElements: '<',
    pageChanged: '&'
  };
  static controller = PaginatorController;
  static template = require('./paginator.component.html');
}
