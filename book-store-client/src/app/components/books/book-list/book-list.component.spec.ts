import * as angular from 'angular';
import 'angular-mocks';
import { BookListComponent } from './book-list.component';

describe('BookList component', () => {
  beforeEach(() => {
    angular
      .module('app', [])
      .component(BookListComponent.selector, BookListComponent)
    angular.mock.module('app');
  });

  it('should exist', angular.mock.inject(($componentController: any) => {
    const component = $componentController(BookListComponent.selector, {}, {});

    expect(component).toBeDefined();
  }));
});
