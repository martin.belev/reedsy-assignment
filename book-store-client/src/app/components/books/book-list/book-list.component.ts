import './book-list.component.scss';
import Book from '../../../models/book.model';

class BookListController implements angular.IComponentController {
  books: Book[];
}

export class BookListComponent implements angular.IComponentOptions {
  static selector = 'bookList';
  static bindings = {
    books: '<',
  };
  static controller = BookListController;
  static template = require('./book-list.component.html');
}
