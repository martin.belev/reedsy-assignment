import './books.container.scss';

import { BooksService } from '../../services/books.service';
import Book from '../../models/book.model';
import PageResult from '../../models/page-result';

class BooksController implements angular.IComponentController {
  books: Book[];
  booksTotalCount: number;

  constructor(
    private booksService: BooksService
  ) {
    'ngInject';
  }

  $onInit() {
    this.fetchBooksPage();
  }

  pageChanged(pageIndex: number, pageSize: number) {
    this.fetchBooksPage(pageIndex, pageSize);
  }

  private fetchBooksPage(pageIndex: number = 0, pageSize: number = 5) {
    this.booksService.getBooks(pageIndex, pageSize).then((result: PageResult<Book>) => {
      this.books = result.value;
      this.booksTotalCount = result.totalCount;
    });
  }
}

export class BooksContainer implements angular.IComponentOptions {
  static selector = 'books';
  static controller = BooksController;
  static template = require('./books.container.html');
}
