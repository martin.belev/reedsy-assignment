import * as angular from 'angular';
import 'angular-mocks';
import { BooksContainer } from './books.container';

describe('Books container', () => {
  let _booksService = {
    getBooks: jasmine.createSpy('getBooks')
  };

  beforeEach(() => {
    angular
      .module('app', [])
      .component(BooksContainer.selector, BooksContainer)
      .value('booksService', _booksService);
    angular.mock.module('app');
  });

  it('should exist', angular.mock.inject(($componentController: any) => {
    const component = $componentController(BooksContainer.selector, {}, {});

    expect(component).toBeDefined();
  }));

  it('should call `booksService.getBooks` on init', angular.mock.inject((
    $componentController: any,
    booksService: any,
    $q: angular.IQService) => {
    const component = $componentController(BooksContainer.selector, {}, {});
    _booksService.getBooks.and.returnValue($q.resolve());
    component.$onInit();

    expect(booksService.getBooks).toHaveBeenCalled();
  }));
});
