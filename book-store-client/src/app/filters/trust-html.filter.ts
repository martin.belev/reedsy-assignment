export class TrustHtmlFilter {
  static selector = 'trustHtml';

  static filter($sce: angular.ISCEService) {
    return (value: string) => {
      if (!value) {
        return value;
      }
      return $sce.trustAsHtml(value);
    };
  }
}
