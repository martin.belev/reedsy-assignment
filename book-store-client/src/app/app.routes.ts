import { BooksContainer } from './components/books/books.container';
import { App } from './app.component';

export const routing = ($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider) => {
  'ngInject';
  $stateProvider
    .state('app', {
      abstract: true,
      url: '',
      component: App.selector
    })

    .state('app.books', {
      url: '/books',
      component: BooksContainer.selector
    });

  $urlRouterProvider.otherwise('/books');
};
