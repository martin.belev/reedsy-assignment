import * as angular from 'angular';
import 'angular-mocks';
import { BooksService } from './books.service';
// import { booksData } from '../data/books.data';
// import PageResult from '../models/page-result';
// import Book from '../models/book.model';

describe('Books service', () => {
  beforeEach(() => {
    angular
      .module('app', [])
      .service('booksService', BooksService);
    angular.mock.module('app');
  });

  it('should exist', angular.mock.inject((booksService: BooksService) => {
    expect(booksService).toBeDefined();
  }));
});
