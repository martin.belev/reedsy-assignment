import Book from '../models/book.model';
import PageResult from '../models/page-result';
import { booksData } from '../data/books.data';

export class BooksService {
  static selector = 'booksService';
  private books: Book[] = booksData;

  constructor(
    private $q: angular.IQService
  ) {
    'ngInject';
  }

  getBooks(pageIndex: number = 0, pageSize: number = 5): angular.IPromise<PageResult<Book>> {
    const booksPage = this.books.slice(pageIndex * pageSize, pageIndex * pageSize + pageSize);
    return this.$q.resolve({
      value: booksPage,
      totalCount: this.books.length
    });
  }
}
