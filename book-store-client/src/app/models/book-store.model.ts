export default interface BookStore {
  name: string;
  buyUrl: string;
}
