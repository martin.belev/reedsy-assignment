import BookStore from './book-store.model';

export default interface Book {
  id: number;
  title: string;
  author: string;
  publishYear: number;
  rating: number;
  imageUrl: string;
  description?: string;
  stores?: BookStore[];
}
