export default interface PageResult<T> {
  value: T[];
  totalCount: number;
}
