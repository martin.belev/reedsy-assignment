import Book from '../models/book.model';

export const booksData: Book[] = [
  {
    id: 1, title: 'In Search of Lost Time', author: 'Marcel Proust', publishYear: 1913, rating: 9.8,
    imageUrl: 'https://images.gr-assets.com/books/1452956236l/12749.jpg',
    stores: [
      { name: 'Amazon', buyUrl: 'https://www.amazon.com/Search-Lost-Time-All-Volumes-ebook/dp/B0771PZY62' },
      { name: 'Play Store', buyUrl: 'https://play.google.com/store/books/details/Marcel_Proust_Swann_s_Way?id=RSLvYXaHbtoC&hl=en_US' }
    ],
    description: `
      <b>In Search of Lost Time</b> (French: À la recherche du temps perdu)—previously also translated as Remembrance of Things Past—is a novel in seven volumes, written by Marcel Proust (1871–1922). It is considered to be his most prominent work, known both for its length and its theme of involuntary memory, the most famous example being the "episode of the madeleine" which occurs early in the first volume. It gained fame in English in translations by C. K. Scott Moncrieff and Terence Kilmartin as Remembrance of Things Past, but the title In Search of Lost Time, a literal rendering of the French, has gained usage since D. J. Enright adopted it for his revised translation published in 1992.

      In Search of Lost Time follows the narrator's recollections of childhood and experiences into adulthood during late 19th century to early 20th century aristocratic France, while reflecting on the loss of time and lack of meaning to the world.[1] The novel began to take shape in 1909. Proust continued to work on it until his final illness in the autumn of 1922 forced him to break off. Proust established the structure early on, but even after volumes were initially finished he kept adding new material and edited one volume after another for publication. The last three of the seven volumes contain oversights and fragmentary or unpolished passages, as they existed only in draft form at the death of the author; the publication of these parts was overseen by his brother Robert.

      The work was published in France between 1913 and 1927. Proust paid for the publication of the first volume (by the Grasset publishing house) after it had been turned down by leading editors who had been offered the manuscript in longhand. Many of its ideas, motifs and scenes are foreshadowed in Proust's unfinished novel, Jean Santeuil (1896–99), though the perspective and treatment there are different, and in his unfinished hybrid of philosophical essay and story, Contre Sainte-Beuve (1908–09).

      The novel had great influence on twentieth-century literature; some writers have sought to emulate it, others to parody it. In the centenary year of the novel's first volume, Edmund White pronounced À la recherche du temps perdu "the most respected novel of the twentieth century".[2]
    `
  },
  {
    id: 2, title: 'Don Quixote', author: 'Miguel de Cervantes', publishYear: 1605, rating: 9.7,
    imageUrl: 'https://wordery.com/jackets/72a7795f/don-quixote-miguel-de-cervantes-9780060934347.jpg',
    stores: [
      { name: 'Amazon', buyUrl: 'https://www.amazon.com/Don-Quixote-Miguel-Cervantes/dp/0060934344' }
    ],
    description: `
      <b>The Ingenious Gentleman Sir Quixote of La Mancha</b> (Modern Spanish: El Ingenioso Hidalgo Don Quijote de la Mancha, pronounced [el iŋxeˈnjoso iˈðalɣo ðoŋ kiˈxote ðe la ˈmantʃa]), or just Don Quixote (/ˌdɒn kiːˈhoʊti/, US: /-teɪ/,[1] Spanish: [doŋ kiˈxote] (About this soundlisten)), is a Spanish novel by Miguel de Cervantes. Published in two parts, in 1605 and 1615, Don Quixote is the most influential work of literature from the Spanish Golden Age and the entire Spanish literary canon. As a founding work of modern Western literature, it regularly appears high on lists of the greatest works of fiction ever published, such as the Bokklubben World Library collection that cites Don Quixote as the authors' choice for the "best literary work ever written".[2]

      The story follows the adventures of a noble (hidalgo) named Alonso Quixano who reads so many chivalric romances that he loses his sanity and decides to become a knight-errant (caballero andante), reviving chivalry and serving his country, under the name Don Quixote de la Mancha. He recruits a simple farmer, Sancho Panza, as his squire, who often employs a unique, earthy wit in dealing with Don Quixote's rhetorical orations on antiquated knighthood. Don Quixote, in the first part of the book, does not see the world for what it is and prefers to imagine that he is living out a knightly story.

      Throughout the novel, Cervantes uses such literary techniques as realism, metatheatre, and intertextuality. The book had a major influence on the literary community, as evidenced by direct references in Alexandre Dumas' The Three Musketeers (1844), Mark Twain's Adventures of Huckleberry Finn (1884), and Edmond Rostand's Cyrano de Bergerac (1897), as well as the word quixotic and the epithet Lothario; the latter refers to a character in "El curioso impertinente" ("The Impertinently Curious Man"), an intercalated story that appears in Part One, chapters 33–35. The 19th-century German philosopher Arthur Schopenhauer cited Don Quixote as one of the four greatest novels ever written, along with Tristram Shandy, La Nouvelle Héloïse, and Wilhelm Meisters Lehrjahre.[3]

      When first published, Don Quixote was usually interpreted as a comic novel. After the French Revolution, it was better known for its central ethic that individuals can be right while society is quite wrong and seen as disenchanting. In the 19th century, it was seen as a social commentary, but no one could easily tell "whose side Cervantes was on". Many critics came to view the work as a tragedy in which Don Quixote's idealism and nobility are viewed by the post-chivalric world as insane, and are defeated and rendered useless by common reality. By the 20th century, the novel had come to occupy a canonical space as one of the foundations of modern literature.
    `
  },
  {
    id: 3, title: 'Ulysses', author: 'James Joyce', publishYear: 1904, rating: 9.5,
    imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/51wTLf4JVwL._SX384_BO1,204,203,200_.jpg',
    stores: [
      { name: 'Amazon', buyUrl: 'https://www.amazon.com/Ulysses-James-Joyce/dp/1494405490' },
      { name: 'iBooks', buyUrl: 'https://books.apple.com/us/book/ulysses/id431259026' }
    ]
  },
  {
    id: 4, title: 'The Great Gatsby', author: 'F. Scott Fitzgerald', publishYear: 1925, rating: 9.4,
    imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/81Y2m%2BfNCbL.jpg',
    description: `
      <b>The Great Gatsby</b> is a 1925 novel written by American author F. Scott Fitzgerald that follows a cast of characters living in the fictional towns of West Egg and East Egg on prosperous Long Island in the summer of 1922.
      The story primarily concerns the young and mysterious millionaire Jay Gatsby and his quixotic passion and obsession with the beautiful former debutante Daisy Buchanan.
      Considered to be Fitzgerald's magnum opus, The Great Gatsby explores themes of decadence, idealism, resistance to change, social upheaval, and excess, creating a portrait of the Roaring Twenties that has been described as a cautionary tale regarding the American Dream.

      First published by Scribner's in April 1925, The Great Gatsby received mixed reviews and sold poorly; in its first year, the book sold only 20,000 copies.
      Fitzgerald died in 1940, believing himself to be a failure and his work forgotten.
      However, the novel experienced a revival during World War II, and became a part of American high school curricula and numerous stage and film adaptations in the following decades.
      Today, The Great Gatsby is widely considered to be a literary classic and a contender for the title "Great American Novel."
    `
  },
  {
    id: 5, title: 'Moby Dick', author: 'Herman Melville', publishYear: 1851, rating: 9.3,
    imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/51oCZKH93sL._SX310_BO1,204,203,200_.jpg'
  },

  {
    id: 6, title: 'Don Quixote', author: 'Miguel de Cervantes', publishYear: 1605, rating: 9.7,
    imageUrl: 'https://wordery.com/jackets/72a7795f/don-quixote-miguel-de-cervantes-9780060934347.jpg'
  },
  {
    id: 7, title: 'Ulysses', author: 'James Joyce', publishYear: 1904, rating: 9.5,
    imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/51wTLf4JVwL._SX384_BO1,204,203,200_.jpg'
  },
  {
    id: 8, title: 'Moby Dick', author: 'Herman Melville', publishYear: 1851, rating: 9.3,
    imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/51oCZKH93sL._SX310_BO1,204,203,200_.jpg'
  },
  {
    id: 9, title: 'In Search of Lost Time', author: 'Marcel Proust', publishYear: 1913, rating: 9.8,
    imageUrl: 'https://images.gr-assets.com/books/1452956236l/12749.jpg'
  },
  {
    id: 10, title: 'Don Quixote', author: 'Miguel de Cervantes', publishYear: 1605, rating: 9.7,
    imageUrl: 'https://wordery.com/jackets/72a7795f/don-quixote-miguel-de-cervantes-9780060934347.jpg'
  },

  {
    id: 11, title: 'The Great Gatsby', author: 'F. Scott Fitzgerald', publishYear: 1925, rating: 9.4,
    imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/81Y2m%2BfNCbL.jpg',
    description: `
      <b>The Great Gatsby</b> is a 1925 novel written by American author F. Scott Fitzgerald that follows a cast of characters living in the fictional towns of West Egg and East Egg on prosperous Long Island in the summer of 1922.
      The story primarily concerns the young and mysterious millionaire Jay Gatsby and his quixotic passion and obsession with the beautiful former debutante Daisy Buchanan.
      Considered to be Fitzgerald's magnum opus, The Great Gatsby explores themes of decadence, idealism, resistance to change, social upheaval, and excess, creating a portrait of the Roaring Twenties that has been described as a cautionary tale regarding the American Dream.

      First published by Scribner's in April 1925, The Great Gatsby received mixed reviews and sold poorly; in its first year, the book sold only 20,000 copies.
      Fitzgerald died in 1940, believing himself to be a failure and his work forgotten.
      However, the novel experienced a revival during World War II, and became a part of American high school curricula and numerous stage and film adaptations in the following decades.
      Today, The Great Gatsby is widely considered to be a literary classic and a contender for the title "Great American Novel."
    `
  }
];
